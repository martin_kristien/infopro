import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class Main{
	public static String address = "http://loys.cardberg.com:80";
	// public static String address = "http://localhost:8000";
	public static String endpoint;
	public static String apikey;
	public static String filename;
	public static String id;
	public static String value;
	public static String bill_id;
	public static String type;
	public static String url;
	public static void main(String [] args){
		if(args.length<3){
			System.out.println("chyba: chybaju parametre");
			return;
		}
		//argument 1
		apikey = args[0];

		//argument 3
		filename = args[2];

		//argument 2
		try{
			switch(args[1]){
				case "1":
					//create card
					if(args.length<6){
						System.out.println("chyba: nedostatok argumentov pre danu funkciu");
						return;
					}
					endpoint = "/api/af/create_gift_card";
					id = args[3];
					value = args[4];
					bill_id = args[5];
					sendCreateCard();
					break;
				case "2":
					//card info
					if(args.length<4){
						System.out.println("chyba: nedostatok argumentov pre danu funkciu");
						return;
					}
					endpoint = "/api/af/card_info";
					id = args[3];
					sendCardInfo();
					break;
				case "3":
					//transaction
					if(args.length<7){
						System.out.println("chyba: nedostatok argumentov pre danu funkciu");
						return;
					}
					endpoint = "/api/af/create_transaction";
					id = args[3];
					value = args[4];
					bill_id = args[5];
					type = args[6];
					sendTransaction();
					break;
				case "4":
					//ping
					endpoint = "/api/af/ping";
					sendPing();
					break;
				default:
					System.out.println("Nepoznam funkciu: druhy argument je "+args[1]);
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		

	}
	private static void sendTransaction()throws Exception{
		//post
		//id, value, type, bill_id in Json body
		url = address + endpoint + "?api_key="+apikey;
		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept", "application/json");
		con.setDoOutput(true);
		String body = "{";
		body += "\"id\": \"" + id + "\",";		//"id": "string",
		body += "\"type\": \"" + type + "\",";	//"type": "string",
		body += "\"value\": \"" + value + "\",";//"value": "double",
		body += "\"bill_id\": \"" + bill_id + "\"";	//"bill_id": "string"
		body += "}";
		con.setRequestProperty("Content-Length", ""+body.length());
		con.setRequestProperty("Content-Type", "application/json");
		System.out.println("url:\n"+url);
		OutputStream out = con.getOutputStream();
		sendBody(out, body);
		int responseCode = con.getResponseCode();
		if(responseCode==200){
			storeFile(con.getInputStream(),filename);
		}else{
			System.out.println("chyba pri posielani requestu, reponse code: "+responseCode);
		}
	}
	private static void sendCreateCard()throws Exception{
		//post
		//id, value, bill_id in Json body
		url = address + endpoint + "?api_key="+apikey;
		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept", "application/json");
		con.setDoOutput(true);
		
		String body = "{";
		body += "\"id\": \"" + id + "\",";		//"id": "string",
		body += "\"value\": \"" + value + "\",";//"value": "double",
		body += "\"bill_id\": \"" + bill_id + "\"";	//"bill_id": "string"
		body += "}";
		con.setRequestProperty("Content-Length", ""+body.length());
		con.setRequestProperty("Content-Type", "application/json");
		System.out.println("url:\n"+url);
		OutputStream out = con.getOutputStream();
		sendBody(out, body);
		int responseCode = con.getResponseCode();
		if(responseCode==200){
			storeFile(con.getInputStream(),filename);
		}else{
			System.out.println("chyba pri posielani requestu, reponse code: "+responseCode);
		}
	}
	private static void sendCardInfo()throws Exception{
		//get
		//id as parameter
		url = address + endpoint + "?id="+id+"&api_key="+apikey;
		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Accept", "application/json");
		System.out.println("url:\n"+url);
		int responseCode = con.getResponseCode();
		if(responseCode == 200){
			storeFile(con.getInputStream(), filename);
		}else{
			System.out.println("chyba pri posielani requestu, reponse code: "+responseCode);
		}
	}
	private static void sendPing()throws Exception{
		//get
		url = address + endpoint + "?api_key="+apikey;
		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Accept", "application/json");
		System.out.println("url:\n"+url);
		int responseCode = con.getResponseCode();
		if(responseCode == 200){
			storeFile(con.getInputStream(), filename);
		}else{
			System.out.println("chyba pri posielani requestu, reponse code: "+responseCode);
		}
	}
	private static void storeFile(InputStream in, String path)throws Exception{
		OutputStream outputStream = null;
		File outputFile = new File(path);
		if(path.indexOf("\\")!=-1){
			outputFile.getParentFile().mkdirs();
		}
		
		outputStream = new FileOutputStream(outputFile);
 
		int read = 0;
		int size = 32;
		byte[] bytes = new byte[size];
		System.out.println("ukladam odozvu");
		String data = "";
 		while ( (read  = in.read(bytes, 0, size)) != -1) {
		  	byte[] bites = new byte[read];
		  	for(int i=0;i<read;i++){
		  		bites[i] = bytes[i];
		  	}
		  	String temp = new String(bites);
 			data += temp;
		}
		data = readUTF(data);
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(data);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String printing = gson.toJson(je);
		data = printing.replace("\n","\r\n");
		System.out.println("data: "+data);
		bytes = data.getBytes();
		outputStream.write(bytes,0,bytes.length);
		outputStream.close();
	}
	private static void sendBody(OutputStream out, String data)throws Exception{
		System.out.println("body:\n"+data);
		out.write(data.getBytes());
		out.close();
		System.out.println("poslane");
	}
	private static String readUTF(String str){
		int last = 0;
		String special;
		int hexVal;
		String newString = "";
		int i;
		while((i=str.indexOf("\\u",last))!=-1){
			newString += str.substring(last,i);
			special = str.substring(i+2,i+6);
			hexVal = Integer.parseInt(special, 16);
			// System.out.println("hexval: "+hexVal);
    		newString += (char)hexVal;
			last = i+6;
		}
		newString += str.substring(last, str.length());
		return newString;
	}
}