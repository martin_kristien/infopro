import javax.json.*;
import java.io.*;
import java.util.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONObject;
import org.json.XML;
public class MegaConverter{
	public static void XmlToJson(InputStream in, OutputStream out) throws Exception{
		String xml = getString(in);
		String json = simpleJson(xml);
		storeJson(json,out);
	}
	public static void main(String [] args) throws Exception{
		File xmlFile = new File("convert.xml");
		File jsonFile = new File("convert.json");
		InputStream in;
		OutputStream out;
		try{
			in = new FileInputStream(xmlFile);
			out = new FileOutputStream(jsonFile);
			XmlToJson(in, out);
		}catch(Exception ee){
			try{
				in = new FileInputStream(jsonFile);
				out = new FileOutputStream(xmlFile);
				JsonToXml(in, out);
			}catch(Exception e){
				e.printStackTrace();
				return;
			}
		}
		
		
	}
	// public static void XmlToJson(InputStream in, OutputStream out) throws Exception{
	// 	String xml = getString (in);
	// 	String json = simpleJson(xml);
	// 	json = json.replace("id", "ean");
	// 	storeJson(json, out);
	// }
	public static void JsonToXml(InputStream in, OutputStream out) throws Exception{
		String json = getString(in);
		String xml = simpleXml(json);
		storeXml(xml, out);
	}
	private static String getString(InputStream in) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line;
		String input="";
		while((line=reader.readLine())!=null){
			input += line + "\r\n";
		}
		return input;
	}
	public static String simpleXml(String json) throws Exception{
		JSONObject o = new JSONObject(json);
		String xml = XML.toString(o);
		xml = xml.replace("><",">\r\n<");
		return xml;
	}
	public static String simpleJson(String xml)throws Exception{
		JSONObject json = XML.toJSONObject(xml);
		String jsonString = json.toString();
		return jsonString;
	}
	
	
	private static void storeJson(String outData, OutputStream out) throws Exception{
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(outData);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String printing = gson.toJson(je);
		String outString = printing.replace("\n","\r\n");
		outString = writeUTF(outString);
	 	out.write(outString.getBytes("UTF-8"));
	}
	private static void storeJson(JsonObject data, OutputStream out) throws Exception{
		String outData = data.toString();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(outData);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String printing = gson.toJson(je);
		String outString = "  " + printing.replace("\n","\r\n");
		outString = writeUTF(outString);
	 	out.write(outString.getBytes("UTF-8"));
	}
	private static void storeXml(String xml, OutputStream out) throws Exception{
		xml = readUTF(xml);
		out.write(xml.getBytes("Windows-1250"));
	}
	private static String writeUTF(String str) {
	  	StringBuilder retStr = new StringBuilder();
	  	for(int i=0; i<str.length(); i++) {
		    int cp = Character.codePointAt(str, i);
		    int charCount = Character.charCount(cp);
		    if (charCount > 1) {
		      i += charCount - 1; // 2.
		      if (i >= str.length()) {
		        //throw new IllegalArgumentException("truncated unexpectedly");
		        System.out.println("truncated unexpectedly");
		      }
		    }

		    if (cp < 128) {
		      retStr.appendCodePoint(cp);
		    } else {
		      retStr.append(String.format("\\u%04x", cp));
		    }
	  	}
	  	return retStr.toString();
	}
	private static String readUTF(String str){
		int last = 0;
		String special;
		int hexVal;
		String newString = "";
		int i;
		while((i=str.indexOf("\\u",last))!=-1){
			newString += str.substring(last,i);
			special = str.substring(i+2,i+6);
			hexVal = Integer.parseInt(special, 16);
    		newString += (char)hexVal;
			last = i+6;
		}
		newString += str.substring(last, str.length());
		return newString;
	}
	
}