import com.sun.net.httpserver.*;
import java.net.InetSocketAddress;
import java.io.*;
import java.util.concurrent.Executors;
import java.util.*;
public class Server{
	public static String prolPath, expPath, impPath;
	private static int port;
	public Server(){
		try{
			loadConfig();
			if(prolPath==null || expPath == null || impPath == null){
				System.out.println("chyba cesta k prol.exe alebo cesta k EXPORT alebo cesta k IMPORT");
				System.exit(0);
			}
			HttpServer server = HttpServer.create(new InetSocketAddress(port),0);

			if(server == null){
				System.out.println("server sa neda vytvorit");
				return;
			}
			appendContexts(server);
			server.setExecutor(Executors.newCachedThreadPool());
			server.start();
			System.out.println("Server vytvoreny, pocuva na porte: "+port);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	private void appendContexts(HttpServer server) throws Exception{
		File urlFile = new File("config\\url.txt");
		InputStream in;
		try{
			in = new FileInputStream(urlFile);
		}catch(Exception e){
			System.out.println("nemohol som najst subor \"config\\url.txt\"\nnemozem nacitat url");
			return;
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line = "";
		String [] lines;
		String url;
		MyContext temp;
		Map<String,MyContext> map = new HashMap<String,MyContext>();
		while((line=reader.readLine())!=null){
			if(!line.trim().equals("")){
				lines = line.split(";");
				if(lines.length!=5){
					System.out.println("zle formovany riadok:\n"+line);
					continue;
				}
				url = lines[0];
				temp = map.get(url);
				if(lines[1].toLowerCase().equals("get")){
					if(temp==null){
						map.put(url,new MyContext("get", lines[2], lines[3], lines[4]));
					}else{
						temp.setGet(lines[2],lines[3], lines[4]);
						map.put(url,temp);
					}
					
				}else if(lines[1].toLowerCase().equals("post")){
					if(temp == null){
						map.put(url,new MyContext("post", lines[2], lines[3],lines[4]));
					}else{
						temp.setPost(lines[2],lines[3], lines[4]);
						map.put(url,temp);
					}
				}else{
					System.out.println("neznama metoda, riadok bude ignorovany:\n"+line);
				}
			}
		}		
		for(String key:map.keySet()){
			server.createContext(key, map.get(key));
		}
	}
	private static void loadConfig() throws Exception{
		File config = new File("config\\server.txt");
		InputStream in = new FileInputStream(config);
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
		String line = reader.readLine();
		String [] info;
		while(line!=null){
			info = line.split(";");
			if(info.length==2){
				switch(info[0]){
					case "host port":
						port = Integer.parseInt(info[1]);
						break;
					case "cesta k prol.exe":
						prolPath = info[1];
						break;
					case "cesta k Import":
						impPath = info[1];
						break;
					case "cesta k Export":
						expPath = info[1];
						break;
				}
			}
			line = reader.readLine();
		}
		reader.close();
	}
}