import com.sun.net.httpserver.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.*;
import java.nio.file.*;
public class MyContext implements HttpHandler{
	String getArgs, postArgs;
	String getPath, postPath;
	InputStream in;
	boolean getConv=false;
	boolean postConv=false;
	public MyContext(String method, String args, String path, String conv){
		switch(method){
			case "get":
				getArgs = args;
				getPath = path;
				if(conv.equals("T")){
					getConv = true;
				}else if(conv.equals("F")){
					getConv = false;
				}else{
					System.out.println("neznamy priznak : "+conv+"\nconverzia bude vypnuta");
				}
				break;
			case "post":
				postArgs = args;
				postPath = path;
				if(conv.equals("T")){
					postConv = true;
				}else if(conv.equals("F")){
					postConv = false;
				}else{
					System.out.println("neznamy priznak : "+conv+"\nconverzia bude vypnuta");
				}
				break;
		}
	}
	public void handle(HttpExchange data){
		try{
			System.out.print("Prijimam request, cas: ");
			printTime();

			String path = data.getRequestURI().getPath();
			System.out.println("path: "+path);

			String method = data.getRequestMethod();
			System.out.println("method: "+method); 
			in = data.getRequestBody();
			int result;
			switch(method){
				case "POST":
					result = createImportFile();
					if(result==1){
						data.sendResponseHeaders(400,-1);
						return;
					}else if (result==2){
						data.sendResponseHeaders(500,-1);
						return;
					}else if(result==3){
						data.sendResponseHeaders(405,-1);
					}
					result = runProl(method);
					if(result==0){
						data.sendResponseHeaders(201,-1);
					}
					break;
				case "GET":
					result = runProl(method);
					if(result==0)
						result = sendBackExportFile( data);
					else{
						data.sendResponseHeaders(500,-1);
						return;
					}
					break;
				default:
					System.out.println("divna method: "+method);
					data.sendResponseHeaders(405,-1);
					return;
			}
			System.out.println("komunikacia ukoncena OK\r\n");
		}catch(Exception e){
			e.printStackTrace();
			try{
				data.sendResponseHeaders(500,-1);
			}catch(Exception ee){
				ee.printStackTrace();
			}
		}
	}
	public int createImportFile()throws Exception{
		String fileName = getNextImportName(Server.impPath+postPath);
		File xmlFile = new File(Server.impPath + postPath + "\\"+fileName+".xml");
		xmlFile.getParentFile().mkdirs();
		OutputStream out = new FileOutputStream(xmlFile);
		if(postConv){
			MegaConverter.JsonToXml(in, out);
		}else{
			copy(in, out);
		}
		out.close();
		in.close();
		return 0;
	}
	public int runProl(String method)throws Exception{
		String args="";
		switch(method){
			case "GET":
				args = getArgs;
				break;
			case "POST":
				args = postArgs;
				break;
		}
		if(args.equals("")){
			return 0;
		}
		System.out.print("zapinam prol.exe "+args +"   cas: ");
		printTime();
		ProcessBuilder builder = new ProcessBuilder(Server.prolPath+"\\prol.exe",args).
			directory(new File(Server.prolPath));
	
		Process process = builder.start();
		int returnValue = process.waitFor();
		System.out.println("prol.exe skoncil s hodnotou: "+returnValue);
		return returnValue;
	}
	public int sendBackExportFile(HttpExchange exchange)throws Exception{
		File xmlFile = new File(Server.expPath + getPath + "\\export.xml");
		InputStream in;
		try{
			in  = new FileInputStream(xmlFile);
		}catch (Exception e){
			exchange.sendResponseHeaders(200,0);
			OutputStream out = exchange.getResponseBody();
			System.out.println("subor s odozvou nenajdeny");
			out.close();
			return 0;
		}
		exchange.sendResponseHeaders(200,0);
		OutputStream out = exchange.getResponseBody();
		if(getConv){
			MegaConverter.XmlToJson(in, out);
		}else{
			copy(in,out);
		}
		in.close();
		out.close();
		Path oldPath = Paths.get(Server.expPath + getPath + "\\export.xml");
		File temp = new File(Server.expPath+getPath+"\\sprac\\export.xml");
		temp.getParentFile().mkdirs();
		Path newPath = Paths.get(Server.expPath + getPath + "\\sprac\\export.xml");

		Files.move(oldPath, newPath, StandardCopyOption.REPLACE_EXISTING);
		System.out.println("odozva poslana");
		return 0;
	}
	private static void copy(InputStream in, OutputStream out)throws Exception{
		byte[] buffer = new byte[1024];
		int len = in.read(buffer);
		while (len != -1) {
		    out.write(buffer, 0, len);
		    len = in.read(buffer);
		}
	}
	public static void printTime(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	   	Date date = new Date();
	   	System.out.println(dateFormat.format(date));
	}
	public String getArgs(){
		return getArgs;
	}
	public String postArgs(){
		return postArgs;
	}
	public String getPath(){
		return getPath;
	}
	public String postPath(){
		return postPath;
	}
	public void setGet(String getArgs, String getPath, String conv){
		this.getPath = getPath;
		this.getArgs = getArgs;
		if(conv.equals("T")){
			getConv = true;
		}else if(conv.equals("F")){
			getConv = false;
		}else{
			System.out.println("neznamy priznak : "+conv+"\nconverzia bude vypnuta");
		}
	}
	public void setPost(String postArgs, String postPath, String conv){
		this.postPath = postPath;
		this.postArgs = postArgs;
		if(conv.equals("T")){
			postConv = true;
		}else if(conv.equals("F")){
			postConv = false;
		}else{
			System.out.println("neznamy priznak : "+conv+"\nconverzia bude vypnuta");
		}
	}
	protected String getNextImportName(String path) throws Exception{
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles(
				new FileFilter(){
					@Override
					public boolean accept(File f){
						if(!f.isFile())
							return false;
						String name = f.getName().split("\\.")[0];
						int temp;
						try{
							temp = Integer.parseInt(name);
						}catch(Exception e){
							return false;
						}
						return true;
					}
				}
			);
		if(listOfFiles.length>0){
			int max = Integer.parseInt(listOfFiles[0].getName().split("\\.")[0]);
		
			int temp;
		    for (File file : listOfFiles) {
			    if (file.isFile()) {
			       	System.out.println("File " + file.getName());
			       	temp = Integer.parseInt(file.getName().split("\\.")[0]);
			       	if(temp>max)
			       		max=temp;
			    }
		    }
		    return ""+(max+1);
		}else return "1";
	}
}