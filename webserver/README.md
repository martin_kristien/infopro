# Infopro Server

This application is designed to provide a web interface to Proluc database system. The main functioning consists of several steps:
* convert json file into xml file as store into Import folder
* run proluc program with specified arguments
* read xml file from Export folder, convert it into json file and send back to client

The application is fully configurable, from server port number, through available urls to locations of import/export folders, proluc.exe file, and arguments for proluc.exe specific to urls.

# Running
The server can be compiled by executing compile.bat file.
The server can be started by executing run.bat file.

# Author
The application was developed by Martin Kristien as part of a summer internship in Infopro, Bratislava, Slovakia in 2015. The application is an abstraction of a piece of software that is currently running on Artforum servers (which was also developed by Martin Kristien). The application language is Slovak.